//
//  EmotionsViewController.swift
//  faceDraw
//
//  Created by Douglas De Voogt on 10/2/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit

class EmotionsViewController: UIViewController {

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        var destinationViewController = segue.destination
        if let navVC = destinationViewController as? UINavigationController {
            destinationViewController = navVC.visibleViewController ?? destinationViewController        }
        if let fvc = destinationViewController as? FaceViewController,
            let identifier = segue.identifier,
            let emotion = emotionsMap[identifier]{
            fvc.expression = emotion
            fvc.navigationItem.title = (sender as? UIButton)?.currentTitle
        }
    }
    
    let emotionsMap: [String:FacialExpression] = [
        "happy": FacialExpression(eyes:.open, mouth:.smile),
        "sad": FacialExpression(eyes:.squinting, mouth:.frown),
        "confused": FacialExpression(eyes:.closed, mouth:.neutral)
    ]
    

}
