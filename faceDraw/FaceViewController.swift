//
//  ViewController.swift
//  faceDraw
//
//  Created by Douglas De Voogt on 9/18/17.
//  Copyright © 2017 Douglas. All rights reserved.
//

import UIKit

class FaceViewController: UIViewController {
    
    @IBOutlet weak var faceView: FaceView!{
        didSet {
            let handler = #selector(FaceView.changeScale(byReactingTo:))
            let pinchRecognizer = UIPinchGestureRecognizer(target: faceView, action: handler)
            faceView.addGestureRecognizer(pinchRecognizer)
            
            let tapHandler = #selector(self.updateEyeState(byReactingTo:))
            let tapRecognizer = UITapGestureRecognizer(target: self, action: tapHandler)
            faceView.addGestureRecognizer(tapRecognizer)
            
            let swipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.increaseHappiness))
            swipeUpRecognizer.direction = .up
            faceView.addGestureRecognizer(swipeUpRecognizer)
            let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(self.decreaseHappiness))
            swipeDownRecognizer.direction = .down
            faceView.addGestureRecognizer(swipeDownRecognizer)
            
            
            
            updateUI()
        }
    }
    
    
    
    @objc func updateEyeState(byReactingTo tapRecognizer: UITapGestureRecognizer){
        if tapRecognizer.state == .ended{
            let eyes: FacialExpression.Eyes = (expression.eyes == .open) ? .closed : .open
            expression = FacialExpression(eyes: eyes, mouth: expression.mouth)
        }
        
    }
    
    @objc func increaseHappiness(){
        expression = expression.happier
    }
    
    @objc func decreaseHappiness() {
        expression = expression.sadder
    }
    
    
    
    var expression = FacialExpression(eyes: .open, mouth: .frown) {
        didSet {
            updateUI()
        }
    }
    
    private func updateUI(){
        switch expression.eyes {
        case .open:
            faceView?.eyeState = "open"
        case .closed:
            faceView?.eyeState = "closed"
        case .squinting:
            faceView?.eyeState = "happy"
        }
        let mouth = expression.mouth
        faceView?.mouthCurvature = mouthCurvature[mouth] ?? 0.0
    }
    
    private let mouthCurvature: [FacialExpression.Mouth:Double] = [
        .frown:-1,
        .smirk:-0.5,
        .neutral:0,
        .grin:0.5,
        .smile:1
    ]
}

